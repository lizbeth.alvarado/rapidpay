﻿using Data.Interfaces;
using Domain.Handlers;
using Domain.Interfaces;
using Domain.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Api.Controllers
{
    [Authorize]
    [Route("cards")]
    public class CardManagementController : Controller
    {
        private readonly ICard _cardRepository;
        private readonly IPayment _paymentRepository;
        private readonly IPaymentFee _paymentFee;

        public CardManagementController(
            ICard cardRepository,
            IPayment paymentRepository,
            IPaymentFee paymentFee
            )
        {
            _cardRepository = cardRepository;
            _paymentRepository = paymentRepository;
            _paymentFee = paymentFee;
        }

        [HttpPost]
        public IActionResult Index([FromBody] Card card)
        {
            try
            {
                return Ok(new CardHandler(_cardRepository).Handle(card));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpPost]
        [Route("pay")]
        public IActionResult Pay([FromBody] Payment payment)
        {
            try
            {
                return Ok(new PaymentHandler(_paymentRepository, _paymentFee).Handle(payment));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }

        [HttpGet]
        [Route("balance/{cardNumber}")]
        public IActionResult Balance(string cardNumber)
        {
            try
            {
                return Ok(new BalanceHandler(_cardRepository).Handle(new Card { CardNumber = cardNumber }));
            }
            catch (Exception ex)
            {
                return BadRequest(new { message = ex.Message });
            }
        }
    }
}
