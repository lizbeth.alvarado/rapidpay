﻿using Data.Interfaces;
using Domain.Exceptions;
using Domain.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Handlers
{
    public class CardHandler : IHandler<Card, bool>
    {
        private readonly ICard _cardRepository;

        public CardHandler(ICard cardRepository)
        {
            _cardRepository = cardRepository;
        }
        public BaseResponse<bool> Handle(Card card)
        {
            var response = new BaseResponse<bool>();

            ExistsCard(_cardRepository.ExistsCard(card.CardNumber));

            _cardRepository.InsertCard(card.CardNumber);
            response.Message = "Card inserted";
            response.Result = true;

            return response;

        }

        private void ExistsCard(bool exists)
        {
            if (exists)
                throw new AlreadyExistsException("Card already exists");
        }
    }

}
