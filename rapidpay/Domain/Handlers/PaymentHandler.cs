﻿using Data.Interfaces;
using Domain.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Handlers
{
    public class PaymentHandler : IHandler<Payment, bool>
    {
        private readonly IPayment _paymentRepository;
        private readonly IPaymentFee _paymentFee;
        public PaymentHandler(IPayment paymentRepository, IPaymentFee paymentFee)
        {
            _paymentRepository = paymentRepository;
            _paymentFee = paymentFee;
        }

        public BaseResponse<bool> Handle(Payment payment)
        {
            var paymentEntity = new Data.Models.Payment
            {
                Card = new Data.Models.Card { CardNumber = payment.CardNumber },
                Amount = payment.Amount,
                Fee = _paymentFee.Calculate()
            };

            _paymentRepository.UpdateBalance(paymentEntity);

            return new BaseResponse<bool>
            {
                Message = "Payment applied",
                Result = true
            };

        }
    }
}
