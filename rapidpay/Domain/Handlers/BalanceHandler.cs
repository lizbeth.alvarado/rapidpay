﻿using Data.Interfaces;
using Domain.Exceptions;
using Domain.Interfaces;
using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Handlers
{
    public class BalanceHandler : IHandler<Card, decimal>
    {
        private readonly ICard _cardRepository;

        public BalanceHandler(ICard cardRepository)
        {
            _cardRepository = cardRepository;
        }

        public BaseResponse<decimal> Handle(Card request)
        {
            ExistsCard(_cardRepository.ExistsCard(request.CardNumber));

            var value = _cardRepository.GetBalance(request.CardNumber);
            return new BaseResponse<decimal>
            {
                Message = "SUCCESS",
                Result = value
            };

        }

        private void ExistsCard(bool exists) 
        {
            if (!exists)
                throw new NotFoundException("Card not found");
        }
    }
}
