﻿using Domain.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Modules
{
    public class PaymentFee : IPaymentFee
    {
        private decimal _paymentFee = 1;
        private DateTime _lastFeeUpdate = DateTime.Now;
        private readonly Random random = new Random();

        public decimal Calculate()
        {
            DateTime current = DateTime.Now;
            if (_paymentFee == 1 || _lastFeeUpdate.AddHours(1) <= current) 
            {
                var randomNumber = Convert.ToDecimal(random.NextDouble() * 2);
                _paymentFee = _paymentFee * randomNumber;
                _lastFeeUpdate = current;
            }

            return _paymentFee;
        }
    }
}
