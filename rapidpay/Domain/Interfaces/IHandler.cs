﻿using Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Interfaces
{
    public interface IHandler<in P, T> where T : struct
    {
        BaseResponse<T> Handle(P entity);
    }
}
