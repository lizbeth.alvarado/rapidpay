﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Domain.Models
{
    public class Card
    {
        [Required]
        [RegularExpression("^[\\d]{15}$", ErrorMessage = "Invalid card")]
        public string CardNumber { get; set; }
    }
}
