﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class CardBalance : Card
    {
        public decimal Balance { get; set; }
    }
}
