﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class BaseResponse<T> where T : struct
    {
        public string Message { get; set; }
        public T Result { get; set; }
    }
}
