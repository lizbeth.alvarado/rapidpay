﻿using Data.Interfaces;
using Data.Models;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Repositories
{
    public class PaymentRepository : IPayment
    {
        public RepositoryContext RepositoryContext { get; set; }
        public PaymentRepository(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public void UpdateBalance(Payment payment)
        {
            var cardNumberParam = new SqlParameter("@CardNumber", payment.Card.CardNumber);
            var amountParam = new SqlParameter("@Amount", payment.Amount);
            var feeParam = new SqlParameter("@Fee", payment.Fee);

            RepositoryContext.Database.ExecuteSqlRaw("exec dbo.UpdateBalance @CardNumber, @Amount, @Fee", cardNumberParam, amountParam, feeParam);
        }
    }
}
