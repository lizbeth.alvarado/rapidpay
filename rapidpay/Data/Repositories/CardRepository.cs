﻿using Data.Interfaces;
using Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Data.Repositories
{
    public class CardRepository : ICard
    {
        public RepositoryContext RepositoryContext { get; set; }

        public CardRepository(RepositoryContext repositoryContext)
        {
            RepositoryContext = repositoryContext;
        }

        public void InsertCard(string cardNumber)
        {
            RepositoryContext.Cards.Add(new Card { CardNumber = cardNumber, Balance = Constants.InitialBalance });
            RepositoryContext.SaveChanges();
        }

        public decimal GetBalance(string cardNumber)
        {
            return RepositoryContext.Cards.Where(x => x.CardNumber == cardNumber).Select(x => x.Balance).FirstOrDefault();
        }

        public bool ExistsCard(string cardNumber)
        {
            return RepositoryContext.Cards.Where(x => x.CardNumber == cardNumber).Count() > 0;
        }
    }
}
