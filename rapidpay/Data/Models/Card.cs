﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Data.Models
{
    [Table("Cards")]
    public class Card
    {
        public Card()
        {
            Payments = new HashSet<Payment>();
        }

        [Key]
        public int CardId { get; set; }

        [Required]
        [StringLength(15)]
        public string CardNumber { get; set; }

        public decimal Balance { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }
    }
}
