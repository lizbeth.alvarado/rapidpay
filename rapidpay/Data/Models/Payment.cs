﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Data.Models
{
    [Table("Payments")]
    public class Payment
    {
        [Key]
        public int PaymentId { get; set; }

        public DateTime PaymentDate { get; set; }

        public decimal Amount { get; set; }


        public decimal Fee { get; set; }

        [ForeignKey(nameof(Card))]
        public int CardId { get; set; }


        public virtual Card Card { get; set; }
    }
}