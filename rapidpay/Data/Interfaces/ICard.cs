﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Data.Interfaces
{
    public interface ICard
    {
        bool ExistsCard(string cardNumber);
        void InsertCard(string cardNumber);
        decimal GetBalance(string cardNumber);
    }
}
