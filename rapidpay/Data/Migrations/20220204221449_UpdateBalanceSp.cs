﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Data.Migrations
{
    public partial class UpdateBalanceSp : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
			var spBody = @"CREATE PROCEDURE dbo.UpdateBalance 
	-- Add the parameters for the stored procedure here
	@CardNumber varchar(15),
	@Amount decimal(18,2),
	@Fee decimal(18,2)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

	DECLARE 
	@FinalBalance decimal(18,2),
	@Balance decimal(18,2),
	@CardId int;

	SELECT @CardId = CardId, @Balance = Balance FROM Cards WHERE CardNumber = @CardNumber;
	SET @FinalBalance = @Balance - (@Amount + @Fee);

    IF (@FinalBalance < 0)
	BEGIN
		;THROW 51000, 'Rejected. Not enough founds', 1;
	END
	ELSE
	BEGIN
		BEGIN TRY
			BEGIN TRANSACTION;
				UPDATE Cards SET Balance = @FinalBalance WHERE CardNumber = @CardNumber;
				INSERT INTO Payments(PaymentDate, Amount, Fee, CardId) VALUES(GetDate(), @Amount, @Fee, @CardId);
			COMMIT TRANSACTION;
		END TRY
		BEGIN CATCH
			ROLLBACK TRANSACTION;
			THROW;
		END CATCH

	END
	
END";

			migrationBuilder.Sql(spBody);
		}

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            // Method intentionally left empty.
        }
    }
}
